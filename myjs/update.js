const pcloud = require('./pcloud')
const fs = require('fs')


function version() {
    let data = fs.readFileSync(__dirname + '\\..\\package.json').toString()
    return JSON.parse(data).version
}

// let git="https://ghproxy.com/"
let git=""
async function version_Server() {
    let data = await pcloud.get(git+"https://gitlab.com/a1246295951/leaguenewway/-/raw/master/package.json")
    return data.version
}

async function checkupdate() {
    console.log(version(), await version_Server())
    return (version() != await version_Server())
    // return

}

async function update(callback) {
    // await pcloud.pcloud_downloadFolder('kZMWepVZPAWWedcfRdBlFdzvOF0TFkDjznEX', __dirname + '\\..\\..\\..\\resources2', (state, p) => {
    //     callback(state, p)
    // })
    await pcloud.downloadFolder(git+'https://gitlab.com/a1246295951/leaguenewway/-/archive/master/leaguenewway-master.zip', __dirname + '\\..\\..\\..\\resources2', (state, p, t, c) => {
        callback(state, p, t, c)
    })
}

module.exports = {
    checkupdate,
    version,
    version_Server,
    update
}